import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        //App.subtask1();
        App.subtask2();
    }

    // hàm tạo 1 array có độ dài 100 kiểu long, convert sang ArrayList<Integer> và in kết quả
    public static void subtask1() {
        System.out.println("Subtask 1: convert array 100 long thành ArrayList<Integer>");
        // tạo array dài 100 kiểu long
        long[] array = new long[100];
        // gán giá trị cho từng phần tử trong array
        for (int i = 0; i < array.length; i++) {
            long longI = i + 1;
            array[i] = longI;
        }
        // tạo ArrayList kiểu Integer
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        for (int i = 0; i < array.length; i++) {
            // gán giá trị cho từng phần tử trong ArrayList
            Integer IntegerI = (int) array[i];
            arrayList.add(IntegerI);
        }
        System.out.println(arrayList.toString());
    }

    // hàm tạo 1 array có độ dài 100 kiểu double, convert sang ArrayList<Double> và in kết quả
    public static void subtask2() {
        System.out.println("Subtask 2: convert array 100 double thành ArrayList<Double>");
        // tạo array dài 100 kiểu double
        double[] array = new double[100];
        // gán giá trị cho từng phần tử trong array
        for (int i = 0; i < array.length; i++) {
            double doubleI = i + 1;
            array[i] = doubleI;
        }
        // tạo ArrayList kiểu Double
        ArrayList<Double> arrayList = new ArrayList<Double>();
        for (int i = 0; i < array.length; i++) {
            // gán giá trị cho từng phần tử trong ArrayList
            Double DoubleI = array[i];
            arrayList.add(DoubleI);
        }
        System.out.println(arrayList.toString());
    }
}
